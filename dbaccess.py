from flask import Flask, jsonify, request
import requests

app = Flask(__name__)

GITLAB_API_TOKEN = 'glpat-rns6WXj7Akp6ZrUGTEXQ'
GITLAB_PROJECT_ID = '57394839'

@app.route('/fetch-credentials', methods=['GET'])
def fetch_credentials():
    job_id = request.args.get('job_id')
    if not job_id:
        return jsonify({"error": "Job ID is required"}), 400

    url = f"https://gitlab.com/api/v4/projects/{GITLAB_PROJECT_ID}/jobs/{job_id}/artifacts/credentials.txt"
    headers = {'PRIVATE-TOKEN': GITLAB_API_TOKEN}
    response = requests.get(url, headers=headers)
    
    if response.status_code == 200:
        credentials = response.text.split(',')
        return jsonify({
            "username": credentials[0].split(': ')[1],
            "password": credentials[1].split(': ')[1],
            "database": credentials[2].split(': ')[1]
        })
    else:
        return jsonify({"error": "Failed to fetch credentials"}), response.status_code

if __name__ == "__main__":
    app.run(debug=True)

